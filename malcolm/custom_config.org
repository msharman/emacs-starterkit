** Add Some colour theming
#+BEGIN_SRC emacs-lisp

(require 'zenburn)
(color-theme-zenburn)
  
#+END_SRC



** Region Expansion
#+BEGIN_SRC emacs-lisp
(add-to-list 'load-path (concat elisp-source-dir "/expand-region.el"))
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)
#+END_SRC



**Chrome support for editing text areas
#+BEGIN_SRC emacs-lisp
;;(require 'edit-server)
;;  (edit-server-start)
  
#+END_SRC


** Rspec mode config for zsh and rvm to work properly
#+BEGIN_SRC emacs-lisp
(defadvice rspec-compile (around rspec-compile-around)
  "Use BASH shell for running the specs because of ZSH issues."
  (let ((shell-file-name "/bin/bash"))
    ad-do-it))
(ad-activate 'rspec-compile)

#+END_SRC 
